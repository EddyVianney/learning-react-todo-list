
const tasks = [

  {
    id: 1,
    content:  "Learning HTML and CSS",
    completed: true
  },

  {
    id: 2,
    content:  "Learning Javascript and Jquery",
    completed: false
  },

  {
    id: 3,
    content:  "Learning MongoDB",
    completed: true
  },

  {
    id: 4,
    content: "Learning Spark SQL",
    completed: false
  },
  {
    id: 5,
    content: "Learning  Angular",
    completed: false
  },

  {
    id: 6,
    content:  "Learning React",
    completed: true
  },
  {
    id: 7,
    content:  "Build first website",
    completed: false
  }
]

export default tasks
