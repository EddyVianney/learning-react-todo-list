import React from "react"
import InputBar from './components/InputBar'
import Task from './components/Task'
import Filter from './components/Filter'
import tasks from './data/tasks'

class App extends React.Component {

  constructor(){
    super()
    this.state = {
      tasks: tasks,
      newTaskContent : ""
    }
    this.removeTask = this.removeTask.bind(this)
    this.addTask = this.addTask.bind(this)
    this.handleClick = this.handleClick.bind(this)
  }

  handleClick(id){
    this.setState(prevState => {
      const newTasks = prevState.tasks.map(task => {
        if (task.id == id){
          return {
               ...task, completed: !task.completed
          }
        }
        return task
      })
      return  {
         tasks: newTasks
      }
    })
  }

  addTask(event) {
    if(event.key == 'Enter') {
    this.setState(prevState => {
      // get the last task
      const lastElement = prevState.tasks.slice(-1)[0]
      return {
            tasks: [...prevState.tasks, {id: lastElement.id + 1, content: event.target.value}]
      }
    })
  }
}

 removeTask(id){
   this.setState(prevState => {
      const newTasks = prevState.tasks.filter(task => task.id != id)

      return {
         tasks : newTasks
      }
   } )

}
  render(){
    const ListTask = this.state.tasks.map(task => <Task key={task.id} task={task} removeTask={this.removeTask} handleClick={this.handleClick} />)
    return (
        <div>
          <InputBar addTask={this.addTask}/>
          <ul>
            {ListTask}
          </ul>
        </div>
    )
  }

}

export default App
