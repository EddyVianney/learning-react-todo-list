import React from "react"

function InputBar(props){

  return (
  <div id="myDIV" className="header">
    <h2>My To Do List</h2>
    <input onKeyPress={props.addTask} type="text" placeholder="Write your task here..." />
    <span className="addBtn">Add</span>
  </div>

  )
}

export default InputBar
