import React from "react"

function Task(props){

  return (
        <li className={props.task.completed ? 'checked': ""} onClick={()=>props.handleClick(props.task.id)}>
        {props.task.content}
        <span onClick={()=> props.removeTask(props.task.id)} className="close">
        &#10006;
        </span></li>
  )
}

export default Task
